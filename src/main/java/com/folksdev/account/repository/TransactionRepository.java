package com.folksdev.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.folksdev.account.model.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, String>{

}
