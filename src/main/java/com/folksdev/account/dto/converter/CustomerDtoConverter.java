package com.folksdev.account.dto.converter;

import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.folksdev.account.dto.AccountCustomerDto;
import com.folksdev.account.dto.CustomerDto;
import com.folksdev.account.model.Customer;

@Component
public class CustomerDtoConverter {

	private final CustomerAccountDtoConverter customerAccountDtoConverter;

	public CustomerDtoConverter(CustomerAccountDtoConverter converter) {
		this.customerAccountDtoConverter = converter;
	}

	public AccountCustomerDto convertToAccountCustomer(Optional<Customer> from) {
        return from.map(f -> new AccountCustomerDto(f.getId(), f.getName(), f.getSurname())).orElse(null);
    }

    public CustomerDto convertToCustomerDto(Customer from) {
        return new CustomerDto(
                from.getId(),
                from.getName(),
                from.getSurname(),
                from.getAccounts()
                    .stream()
                    .map(customerAccountDtoConverter::convert)
                    .collect(Collectors.toSet()));

    }

}
