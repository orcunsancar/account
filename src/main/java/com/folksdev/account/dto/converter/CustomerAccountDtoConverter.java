package com.folksdev.account.dto.converter;

import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.folksdev.account.dto.CustomerAccountDto;
import com.folksdev.account.model.Account;

@Component
public class CustomerAccountDtoConverter {

	private final TransactionDtoConverter transactionDtoConverter;

	public CustomerAccountDtoConverter(TransactionDtoConverter converter) {
		this.transactionDtoConverter = converter;
	}
	
	public CustomerAccountDto convert(Account from) {
        return new CustomerAccountDto(
                Objects.requireNonNull(from.getId()),
                from.getBalance(),
                from.getTransaction()
                        .stream()
                        .map(transactionDtoConverter::convert)
                        .collect(Collectors.toSet()),
                from.getCreationDate());
    }
	
}
