package com.folksdev.account.dto;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class CreateAccountRequest {
	@NotBlank(message = "CustomerId must not be empty")
	private String customerId;
	@Min(message = "Initial Credit value must not be negative value",value = 0)
	private BigDecimal initialCredit;

	public CreateAccountRequest(String customerId, BigDecimal initialCredit) {
		this.customerId = customerId;
		this.initialCredit = initialCredit;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public BigDecimal getInitialCredit() {
		return initialCredit;
	}

	public void setInitialCredit(BigDecimal initialCredit) {
		this.initialCredit = initialCredit;
	}

}
